<?php

// This is used to generate the social url fields in the customizer and the social icons in the template.
$socialKeys = array (
  'facebook',
  'instagram',
  'twitter',
  'youtube',
  'pinterest',
  'linkedin',
);
