<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package dgd-tailwind
 */

if ( ! function_exists( 'dgd_tailwind_posted_on' ) ) :
	/**
	 * Prints HTML with meta information for the current post-date/time.
	 */
	function dgd_tailwind_posted_on() {
		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
		}

		$time_string = sprintf( $time_string,
			esc_attr( get_the_date( DATE_W3C ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_modified_date( DATE_W3C ) ),
			esc_html( get_the_modified_date() )
		);

		$posted_on = sprintf(
			/* translators: %s: post date. */
			esc_html_x( '%s', 'post date', 'dgd-tailwind' ),
			'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
		);

		echo '<span class="posted-on">' . $posted_on . '</span>'; // WPCS: XSS OK.

	}
endif;

if ( ! function_exists( 'dgd_tailwind_posted_by' ) ) :
	/**
	 * Prints HTML with meta information for the current author.
	 */
	function dgd_tailwind_posted_by() {
		$byline = sprintf(
			/* translators: %s: post author. */
			esc_html_x( 'by %s', 'post author', 'dgd-tailwind' ),
			'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
		);

		echo '<span class="byline"> ' . $byline . '</span>'; // WPCS: XSS OK.

	}
endif;

if ( ! function_exists( 'dgd_tailwind_entry_categories_tags' ) ) :
	/**
	 * Prints HTML with meta information for the categories, tags and comments.
	 */
	function dgd_tailwind_entry_categories_tags() {
		// Hide category and tag text for pages.
		if ( 'post' === get_post_type() ) {
			/* translators: used between list items, there is a space after the comma */
			$categories_list = get_the_category_list( esc_html__( ', ', 'dgd-tailwind' ) );
			if ( $categories_list ) {
				/* translators: 1: list of categories. */
				printf( '<span class="cat-links">' . esc_html__( 'Posted in %1$s', 'dgd-tailwind' ) . '</span>', $categories_list ); // WPCS: XSS OK.
			}

			/* translators: used between list items, there is a space after the comma */
			$tags_list = get_the_tag_list( '', esc_html_x( ', ', 'list item separator', 'dgd-tailwind' ) );
			if ( $tags_list ) {
				/* translators: 1: list of tags. */
				printf( '<span class="tags-links">' . esc_html__( 'Tagged %1$s', 'dgd-tailwind' ) . '</span>', $tags_list ); // WPCS: XSS OK.
			}
		}

		if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
			echo '<span class="comments-link">';
			comments_popup_link(
				sprintf(
					wp_kses(
						/* translators: %s: post title */
						__( 'Leave a Comment<span class="screen-reader-text"> on %s</span>', 'dgd-tailwind' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				)
			);
			echo '</span>';
		}

		edit_post_link(
			sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Edit <span class="screen-reader-text">%s</span>', 'dgd-tailwind' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			),
			'<span class="edit-link">',
			'</span>'
		);
	}
endif;

if ( ! function_exists( 'dgd_tailwind_post_thumbnail' ) ) :
	/**
	 * Displays an optional post thumbnail.
	 *
	 * Wraps the post thumbnail in an anchor element on index views, or a div
	 * element when on single views.
	 */
	function dgd_tailwind_post_thumbnail( $containerClasses = null ) {
		if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
			return;
		}

		if ( is_singular() ) :
			?>

			<div class="<?php echo $containerClasses; ?>">
				<?php the_post_thumbnail(); ?>
			</div>

		<?php else : ?>

		<a class="<?php echo $containerClasses; ?>" href="<?php the_permalink(); ?>" aria-hidden="true" tabindex="-1">
			<?php
			the_post_thumbnail( 'post-thumbnail', array(
				'alt' => the_title_attribute( array(
					'echo' => false,
				) ),
			) );
			?>
		</a>

		<?php
		endif; // End is_singular().
	}
endif;

/**
 * Output social icons.
 *
 * @param string $wrapperClasses Classes for the ul wrapper.
 * @param string $itemClasses Classes for each list item.
 * @param string $linkClasses Classes for each link.
 * @return void
 */
function dgd_social_icons( $wrapperClasses, $itemClasses, $linkClasses = '' ) {
	$socialKeys = array (
		'facebook',
		'instagram',
		'twitter',
		'youtube',
		'pinterest',
		'linkedin',
	);

	$socialURLs = array();
	$socialIcons = array();
	foreach ( $socialKeys as $key ) {
		$url = get_theme_mod( $key . '_url' );
		if ( $url ) {
			$socialURLs[] = $url;
			$socialIcons[] = file_get_contents( get_stylesheet_directory_uri() . '/assets/' . $key . '_icon_round.svg' );
		}
	}

	if ( !$socialURLs ) {
		return;
	}

	$wrapperClasses = ( $wrapperClasses ? $wrapperClasses : 'flex' );
	$itemClasses = ( $itemClasses ? $itemClasses : 'h-8 w-8 mr-2' );

	$output = "<ul class=\"$wrapperClasses\">";

	foreach ( $socialURLs as $key => $social ) {
		$output .= "<li class=\"$itemClasses\"><a href=\"$social\" class=\"inline-block icon-wrap social-icon-wrap $linkClasses\" target=\"_blank\">" . $socialIcons[$key] . "<span class=\"sr-only\">Facebook</span></a></li>";
	}

	echo $output;
}

/**
 * dgd_page_header
 *
 * @param string $title
 * @param integer $titleLevel
 * @param string $description
 * @return void
 */
function dgd_page_header( $title, $titleLevel = 1, $description = null ) {
	if ( !$title ) {
		return;
	}
	?>
	<header class="p-6 text-center lg:py-8">
		<div class="m-auto max-w-screen-lg">
			<h<?php echo $titleLevel; ?>><?php echo $title; ?></h<?php echo $titleLevel; ?>>
			<?php if ( $description ) : ?>
				<div class="last-child-m-0 mt-4"><?php echo $description; ?></div>
			<?php endif; ?>
		</div>
	</header>
<?php
}

function dgd_credit() {
	echo '<a href="https://dogood.design" target="_blank" class="text-gray-300">Website Design by Do Good Design Co.</a>';
}
