<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package dgd-tailwind
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function dgd_tailwind_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'dgd_tailwind_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function dgd_tailwind_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">', esc_url( get_bloginfo( 'pingback_url' ) ) );
	}
}
add_action( 'wp_head', 'dgd_tailwind_pingback_header' );

/**
 * dgd_tailwind_calls_to_action_menu_classes
 *
 * @param array $atts menu link attributes
 * @param object $item current menu item
 * @param object $args menu arguments
 * @param int $depth depth level of item
 * @return array
 */
function dgd_tailwind_calls_to_action_menu_link_classes( $atts, $item, $args, $depth) {
	if ( $args->theme_location === 'calls-to-action' ) {
		$atts['class'] = 'button';
	}
	return $atts;
}
add_filter( 'nav_menu_link_attributes', 'dgd_tailwind_calls_to_action_menu_link_classes', 10, 4 );

function dgd_tailwind_calls_to_action_menu_item_classes( $classes, $item, $args, $depth) {
	if ( $args->theme_location === 'calls-to-action' ) {
	  $classes[] = 'ml-2';
	}
  return $classes;
}
add_filter( 'nav_menu_css_class', 'dgd_tailwind_calls_to_action_menu_item_classes', 10, 4 );
