<?php
/**
 * dgd-tailwind Theme Customizer
 *
 * @package dgd-tailwind
 */

/**
 * Check if checkbox is checked. To be used as a callback in the customizer.
 *
 */
function dgd_sanitize_checkbox( $checked ) {
	// Boolean check.
	return ( ( isset( $checked ) && true == $checked ) ? true : false );
}

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function dgd_tailwind_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';

	$wp_customize->add_section( 'header', array(
		'title' => __( 'Header' ),
		'priority' => 160,
		'capability' => 'edit_theme_options',
	) );

	$wp_customize->add_setting( 'top_bar_visibility', array(
		'capability' => 'edit_theme_options',
		'sanitize_callback' => 'dgd_sanitize_checkbox',
	) );

	$wp_customize->add_control( 'top_bar_visibility', array(
		'type' => 'checkbox',
		'priority' => 10,
		'section' => 'header',
		'label' => __( 'Display the top bar?' ),
	) );

	$wp_customize->add_setting( 'header_style', array(
		'capability' => 'edit_theme_options',
		'default' => 'logo-left',
	) );

	$wp_customize->add_control( 'header_style', array(
		'type' => 'select',
		'priority' => 10,
		'section' => 'header',
		'label' => __( 'Header Style' ),
		'choices' => array(
			'logo-left' => __( 'Logo Left' ),
			'logo-centered' => __( 'Logo Centered' ),
		),
	) );

	$wp_customize->add_section( 'footer', array(
		'title' => __( 'Footer' ),
		'priority' => 170,
		'capability' => 'edit_theme_options',
	) );

	$wp_customize->add_setting( 'footer_style', array(
		'capability' => 'edit_theme_options',
		'default' => '4-col',
	) );

	$wp_customize->add_control( 'footer_style', array(
		'type' => 'select',
		'priority' => 10,
		'section' => 'footer',
		'label' => __( 'Footer Style' ),
		'choices' => array(
			'4-col' => __( '4 Columns' ),
			'1-col' => __( '1 Column' ),
		),
	) );

	$wp_customize->add_setting( 'show_logo_in_footer', array(
		'capability'				=> 'edit_theme_options',
		'sanitize_callback'	=> 'dgd_sanitize_checkbox',
		'default'						=> true
	) );

	$wp_customize->add_control( 'show_logo_in_footer', array(
		'type' => 'checkbox',
		'priority' => 15,
		'section' => 'footer',
		'label' => __( 'Display the logo in the footer?' ),
	) );

	$wp_customize->add_setting( 'show_social_in_footer', array(
		'capability'				=> 'edit_theme_options',
		'sanitize_callback'	=> 'dgd_sanitize_checkbox',
		'default'						=> true
	) );

	$wp_customize->add_control( 'show_social_in_footer', array(
		'type' => 'checkbox',
		'priority' => 20,
		'section' => 'footer',
		'label' => __( 'Display the social media icons in the footer?' ),
	) );

	$wp_customize->add_setting( 'email_bar_style', array(
		'capability' => 'edit_theme_options',
		'default' => 'email_social',
	) );

	$wp_customize->add_control( 'email_bar_style', array(
		'type' => 'select',
		'priority' => 25,
		'section' => 'footer',
		'label' => __( 'Email Signup Bar Style' ),
		'choices' => array(
			'none' => 'None',
			'email_only' => __( 'Email Signup Only' ),
			'email_social' => __( 'Email Signup and Social Icons' ),
		),
	) );

	$wp_customize->add_setting( 'email_bar_text', array(
		'capability' => 'edit_theme_options',
		'default' => 'Subscribe today to receive news, updates and to hear about upcoming workshops.',
	) );

	$wp_customize->add_control( 'email_bar_text', array(
		'type' => 'textarea',
		'priority' => 30,
		'section' => 'footer',
		'label' => __( 'Email Signup Bar Text' ),
	) );

	$wp_customize->add_section( 'blog', array(
		'title' => __( 'Blog' ),
		'priority' => 180,
		'capability' => 'edit_theme_options',
	) );

	$wp_customize->add_setting( 'show_post_navigation', array(
		'capability' => 'edit_theme_options',
		'sanitize_callback' => 'dgd_sanitize_checkbox',
	) );

	$wp_customize->add_control( 'show_post_navigation', array(
		'type' => 'checkbox',
		'priority' => 10,
		'section' => 'blog',
		'label' => __( 'Display links to the previous and next posts at the bottom of blog posts?' ),
	) );

	$wp_customize->add_section( 'social', array(
		'title' => __( 'Social' ),
		'priority' => 190,
		'capability' => 'edit_theme_options',
	) );

	$wp_customize->add_setting( 'facebook_url', array(
		'capability' => 'edit_theme_options',
	) );

	$wp_customize->add_control( 'facebook_url', array(
		'type' => 'text',
		'priority' => 10,
		'section' => 'social',
		'label' => __( 'Facebook URL' ),
	) );

	$wp_customize->add_setting( 'instagram_url', array(
		'capability' => 'edit_theme_options',
	) );

	$wp_customize->add_control( 'instagram_url', array(
		'type' => 'text',
		'priority' => 10,
		'section' => 'social',
		'label' => __( 'Instagram URL' ),
	) );

	$wp_customize->add_setting( 'linkedin_url', array(
		'capability' => 'edit_theme_options',
	) );

	$wp_customize->add_control( 'linkedin_url', array(
		'type' => 'text',
		'priority' => 10,
		'section' => 'social',
		'label' => __( 'LinkedIn URL' ),
	) );

	$wp_customize->add_setting( 'twitter_url', array(
		'capability' => 'edit_theme_options',
	) );

	$wp_customize->add_control( 'twitter_url', array(
		'type' => 'text',
		'priority' => 10,
		'section' => 'social',
		'label' => __( 'Twitter URL' ),
	) );

	$wp_customize->add_setting( 'youtube_url', array(
		'capability' => 'edit_theme_options',
	) );

	$wp_customize->add_control( 'youtube_url', array(
		'type' => 'text',
		'priority' => 10,
		'section' => 'social',
		'label' => __( 'Youtube URL' ),
	) );

	$wp_customize->add_setting( 'pinterest_url', array(
		'capability' => 'edit_theme_options',
	) );

	$wp_customize->add_control( 'pinterest_url', array(
		'type' => 'text',
		'priority' => 10,
		'section' => 'social',
		'label' => __( 'Pinterest URL' ),
	) );

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector'        => '.site-title a',
			'render_callback' => 'dgd_tailwind_customize_partial_blogname',
		) );
		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector'        => '.site-description',
			'render_callback' => 'dgd_tailwind_customize_partial_blogdescription',
		) );
	}
}
add_action( 'customize_register', 'dgd_tailwind_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function dgd_tailwind_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function dgd_tailwind_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function dgd_tailwind_customize_preview_js() {
	wp_enqueue_script( 'dgd-tailwind-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'dgd_tailwind_customize_preview_js' );
