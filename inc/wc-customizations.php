<?php

add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

function ee_wc_breadcrumbs( $defaults ) {
	$defaults['wrap_before'] = '<nav class="woocommerce-breadcrumb bg-orange-300 px-2 py-3 text-orange-900 text-sm tracking-wider truncate uppercase whitespace-no-wrap lg:px-4" itemprop="breadcrumb">';
	$defaults['wrap_after'] = '</nav>';
	$defaults['delimiter'] = '<span class="font-light"> / </span>';
	return $defaults;
}
add_filter( 'woocommerce_breadcrumb_defaults', 'ee_wc_breadcrumbs' );

// Wrap the single product summary and tabs with a div for styling.
add_action( 'woocommerce_before_single_product_summary', function() { print '<div class="lg:ml-3 lg:w-1/2">'; }, 30 );
add_action( 'woocommerce_after_single_product_summary', function() { print '</div>'; }, 30 );

// Re-order and ammend product page elements.
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

/*
 * Output just the product's categories, instead of all the meta.
 */
function dgd_output_product_categories() {
	global $product;
	echo '<div class="font-normal">' . $product->get_categories() . '</div>';
}
add_action( 'woocommerce_single_product_summary', 'dgd_output_product_categories', 5 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 10 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 15 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 20 );

/*
 * Output the product's size attribute.
 */
function dgd_output_product_size() {
	global $product;
	echo '<div class="hidden lg:block lg:mb-4"><div class="font-bold text-sm">Size</div>' . $product->get_attribute('size') . '</div>';
}
add_action( 'woocommerce_single_product_summary', 'dgd_output_product_size', 25 );

// also add it to the description so it appears there on small screens
function dgd_product_description_tab( $content ){
	global $product;
	if( is_product() ) {
		$content = '<div class="lg:hidden"><p class="font-bold text-sm">Size</p><p>' . $product->get_attribute('size') . '</p></div>' . $content;
	}
	return $content;
}
add_filter( 'the_content', 'dgd_product_description_tab' );

// Reorganize the product tabs
function dgd_reorganize_tabs( $tabs ) {
    unset( $tabs['additional_information'] );
    unset( $tabs['reviews'] );

    $tabs['application'] = array(
    	'title'		=> 'Application',
    	'priority'	=> 50,
    	'callback'	=> function() {
    		echo '<h2>Application</h2>';
    		echo get_field('application');
    	}
    );

    $tabs['ingredients'] = array(
    	'title'		=> 'Ingredients',
    	'priority'	=> 50,
    	'callback'	=> function() {
    		echo '<h2>Ingredients</h2>';
    		echo get_field('ingredients');
    	}
    );
    return $tabs;
}
add_filter( 'woocommerce_product_tabs', 'dgd_reorganize_tabs', 98 );

/**
 * These style the product tiles,
 * creating a new div and place the product thumbnail into it, to separate it from the rest of the content for styling,
 * among other things.
 */

// Open up a div that contains all the product tile elements except the image, for styling.
function dgd_open_product_inner() {
	echo '<div class="bg-orange-100 flex flex-col flex-grow p-5">';
}
add_action( 'woocommerce_before_shop_loop_item_title', 'dgd_open_product_inner', 10 );

// Close a div that contains all the product tile elements except the image, for styling.
function dgd_close_product_inner() {
	echo '</div>';
}
add_action( 'woocommerce_after_shop_loop_item', 'dgd_close_product_inner', 100 );

// Move the opening link tag so it doesn't encompass the image, only the heading.
remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_link_open', 15 );

/**
 * Change the output of the product thumbnail.
 */
function dgd_woocommerce_get_product_thumbnail( $size = 'shop_catalog' ) {
    global $post, $product, $woocommerce;

    $link = apply_filters( 'woocommerce_loop_product_link', get_the_permalink(), $product );

    $output = '<div class="product_thumbnail_container"><a href="' . esc_url( $link ) . '">';

    if ( has_post_thumbnail() ) {               
        $output .= get_the_post_thumbnail( $post->ID, $size );
    } else {
         $output .= wc_placeholder_img( $size );
         // Or alternatively setting yours width and height shop_catalog dimensions.
         // $output .= '<img src="' . woocommerce_placeholder_img_src() . '" alt="Placeholder" width="300px" height="300px" />';
    }
    $output .= '</a>';
    echo $output;
    woocommerce_template_loop_add_to_cart();
    echo '</div>';
}
remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );
add_action( 'woocommerce_before_shop_loop_item', 'dgd_woocommerce_get_product_thumbnail', 5 );

/**
 * Wrap the star ratings on product tiles for styling
 */
function dgd_woocommerce_template_loop_rating_wrapped() {
	global $product;
	$count = $product->get_review_count();
	if ( $count && wc_review_ratings_enabled() ) {
		echo '<div class="rating border-t border-orange-300 mt-auto pt-4">';
		woocommerce_template_loop_rating();
		echo '</div>';
	}
}
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
add_action( 'woocommerce_after_shop_loop_item_title', 'dgd_woocommerce_template_loop_rating_wrapped', 15 );

// Move the closing link tag so it doesn't encompass the price, only the heading.
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );
add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_product_link_close', 5 );

// Move related products out of the product summary.
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

function dgd_woocommerce_output_related_products() {
	if ( is_product() ) {
		woocommerce_output_related_products();
	}
}
add_action( 'woocommerce_after_main_content', 'dgd_woocommerce_output_related_products', 20 );

// Unhook the add to cart button. It's hooked into the new image function.
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );

/*
 * Output the testimonial associated with this produst via ACF.
 */
function dgd_output_product_testimonial() {
	global $product;

	if ( is_product() ) {
		$testimonial = ( get_field( 'testimonial', $product ) ) ? get_field( 'testimonial', $product ) : get_post(64);

?>
	<div class="bg-green-500 font-serif px-5 py-8 text-center text-xl text-white lg:py-16">
		<div class="mb-4 mx-auto px-5 max-w-4xl"><?php echo $testimonial->post_content; ?></div>
		<div class="font-sans m-auto max-w-4xl px-5 text-base"><?php echo $testimonial->post_title; ?></div>
	</div>
<?php
	}
}
add_action( 'woocommerce_after_main_content', 'dgd_output_product_testimonial', 15 );

/**
 * Change number of related products output
 */ 
function dgd_related_products_args( $args ) {
	$args['posts_per_page'] = 3; // 3 related products
	$args['columns'] = 3; // arranged in 2 columns
	return $args;
}
add_filter( 'woocommerce_output_related_products_args', 'dgd_related_products_args', 20 );

// Output reviews in its own section.
add_action( 'woocommerce_after_main_content', 'comments_template', 25 );

/**
 * Adjustments to the reviews display.
 */

// Remove the rating stars because they were added to the review-meta template.
remove_action( 'woocommerce_review_before_comment_meta', 'woocommerce_review_display_rating', 10 );

/*
 * Archive customizations
 */
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_output_all_notices', 10 );
add_action( 'woocommerce_before_main_content', 'woocommerce_output_all_notices', 40 );

/*
 * Cart
 */
remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display');
remove_action( 'woocommerce_before_cart', 'woocommerce_output_all_notices', 10 );
add_action( 'dgd_woocommerce_output_all_notices', 'woocommerce_output_all_notices', 10 );
add_action( 'woocommerce_before_cart', function() { echo '<div id="cart_wrapper">'; }, 0 );
add_action( 'woocommerce_after_cart', function() { echo '</div>'; }, 100 );

/*
 * Checkout
 */

// wrap the order review heading along with the table
add_action( 'woocommerce_checkout_before_order_review_heading', function() { echo '<div id="review_order_wrapper">'; }, 0 );
add_action( 'woocommerce_checkout_after_order_review', function() { echo '</div>'; }, 100 );

/*
 * Account
 */
remove_action( 'woocommerce_account_navigation', 'woocommerce_account_navigation' );
add_action( 'dgd_woocommerce_account_navigation', 'woocommerce_account_navigation' );
