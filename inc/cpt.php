<?php

// Register Custom Post Type
function dgd_custom_post_types() {

	$labels = array(
		'name'                  => _x( 'Testimonials', 'Post Type General Name', 'dgd' ),
		'singular_name'         => _x( 'Testimonial', 'Post Type Singular Name', 'dgd' ),
		// 'menu_name'             => __( 'Post Types', 'dgd' ),
		// 'name_admin_bar'        => __( 'Post Type', 'dgd' ),
		'archives'              => __( 'Testimonial Archives', 'dgd' ),
		'attributes'            => __( 'Testimonial Attributes', 'dgd' ),
		'parent_item_colon'     => __( 'Parent Testimonial:', 'dgd' ),
		'all_items'             => __( 'All Testimonials', 'dgd' ),
		'add_new_item'          => __( 'Add New Testimonial', 'dgd' ),
		// 'add_new'               => __( 'Add New', 'dgd' ),
		'new_item'              => __( 'New Testimonial', 'dgd' ),
		'edit_item'             => __( 'Edit Testimonial', 'dgd' ),
		'update_item'           => __( 'Update Testimonial', 'dgd' ),
		'view_item'             => __( 'View Testimonial', 'dgd' ),
		'view_items'            => __( 'View Testimonials', 'dgd' ),
		'search_items'          => __( 'Search Testimonials', 'dgd' ),
		// 'not_found'             => __( 'Not found', 'dgd' ),
		// 'not_found_in_trash'    => __( 'Not found in Trash', 'dgd' ),
		'featured_image'        => __( 'Featured Image', 'dgd' ),
		// 'set_featured_image'    => __( 'Set featured image', 'dgd' ),
		// 'remove_featured_image' => __( 'Remove featured image', 'dgd' ),
		// 'use_featured_image'    => __( 'Use as featured image', 'dgd' ),
		'insert_into_item'      => __( 'Insert into Testimonial', 'dgd' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Testimonial', 'dgd' ),
		'items_list'            => __( 'Testimonials list', 'dgd' ),
		'items_list_navigation' => __( 'Testimonials list navigation', 'dgd' ),
		'filter_items_list'     => __( 'Filter Testimonials list', 'dgd' ),
	);
	$args = array(
		'label'                 => __( 'Testimonial', 'dgd' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor' ),
		'taxonomies'            => array(),
		// 'hierarchical'          => false,
		// 'public'                => true,
		'show_ui'               => true,
		// 'show_in_menu'          => true,
		// 'menu_position'         => 5,
		// 'show_in_admin_bar'     => true,
		// 'show_in_nav_menus'     => true,
		// 'can_export'            => true,
		// 'has_archive'           => true,
		'exclude_from_search'   => true,
		// 'publicly_queryable'    => true,
		// 'capability_type'       => 'page',
	);
	register_post_type( 'testimonial', $args );

}
add_action( 'init', 'dgd_custom_post_types', 0 );