<?php
/**
 * Template part for displaying post excerpts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package dgd-tailwind
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'post-excerpt' ); ?>>

  <?php dgd_tailwind_post_thumbnail(); ?>
  
  <div class="p-4">

    <header>
      <?php
      the_title( '<h2 class="h3"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );

      dgd_tailwind_entry_categories_tags();
      get_template_part( 'template-parts/blog/entry-meta' );
      ?>
    </header>

    <div>
      <?php the_excerpt(); ?>
    </div>

    <footer>
      <a class="button" href="<?php echo get_the_permalink(); ?>">Read More</a>
    </footer>
  
  </div>
</article><!-- #post-<?php the_ID(); ?> -->
