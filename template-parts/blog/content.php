<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package dgd-tailwind
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'bg-white m-auto max-w-screen-md' ); ?>>

  <?php dgd_tailwind_post_thumbnail(); ?>
  
  <div class="p-8">

    <header>
      <?php
        the_title( '<h1>', '</h1>' );
        dgd_tailwind_entry_categories_tags();
        get_template_part( 'template-parts/blog/entry-meta' );
      ?>
    </header>
      
    <div>
      <?php 
      the_content();
      ?>
    </div>

    <footer>
    </footer>

  </div>
</article><!-- #post-<?php the_ID(); ?> -->
