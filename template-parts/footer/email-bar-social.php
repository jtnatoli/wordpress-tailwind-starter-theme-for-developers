<div class="bg-gray-700 flex flex-col items-center px-4 py-6 lg:flex-row lg:justify-between lg:py-3">
  <div class="mb-4 text-center lg:mb-0 lg:text-left">
    <div class="block mb-2 lg:inline-block lg:mb-0 lg:mr-4">
      <?php echo get_theme_mod( 'email_bar_text' ); ?>
    </div>
    <input type="email" placeholder="Please enter your email" class="mr-2 w-auto" /><button type="submit" class="button">Join</button>
  </div>
  <?php dgd_social_icons( false, false, 'text-gray-300 hover:text-white' ); ?>
</div>
