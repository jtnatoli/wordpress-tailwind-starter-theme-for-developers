<div class="m-auto max-w-6xl p-6 text-center text-gray-200 lg:flex lg:justify-between lg:text-left">
  <nav class="leading-loose text-sm">
    <div class="font-normal">Footer 1</div>
    <?php
      wp_nav_menu( array(
        'theme_location' => 'footer-1'
      ) );
    ?>
  </nav>
  <nav class="leading-loose text-sm">
    <div class="font-normal">Footer 2</div>
    <?php
      wp_nav_menu( array(
        'theme_location' => 'footer-2'
      ) );
    ?>
  </nav>
  <nav class="leading-loose text-sm">
    <div class="font-normal">Footer 3</div>
    <?php
      wp_nav_menu( array(
        'theme_location' => 'footer-3'
      ) );
    ?>
  </nav>
  <div class="leading-loose text-sm lg:py-0">
    <div class="font-normal text-sm">Contact</div>
    <p><a href="mailto:contact@earthessence.com">contact@earthessence.com</a></p>
    <p>555-555-5555<br />Contact us anytime</p>
    <p class="my-0">2 Lorem Ipsum St, Suite 1<br />San Francisco, CA 94118</p>
  </div>
</div>
<nav class="border-t border-gray-500 p-6 text-xs">
  <?php
    wp_nav_menu( array(
      'theme_location' => 'sub-footer'
    ) );
  ?>
</nav>
<div id="credit" class="p-6 text-center text-xs">
  <p>&copy; 2020 Do Good Design Co. - All Rights Reserved.</p>
  <p class="mb-0 text-gray-300"><a href="https://dogood.design" target="_blank" class="text-gray-300">Website Design by Do Good Design Co.</a></p>
</div>
