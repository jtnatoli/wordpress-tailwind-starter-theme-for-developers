<div class="bg-gray-700 flex justify-center px-4 py-6 lg:py-3">
  <div class="flex flex-col items-center mb-4 w-full lg:flex-row lg:justify-center lg:max-w-9/12 lg:mb-0 xl:max-w-2/3">
    <div class="block mb-2 lg:inline-block lg:mb-0 lg:mr-4 lg:text-right">
      <?php echo get_theme_mod( 'email_bar_text' ); ?>
    </div>
    <div class="flex flex-grow lg:w-8/12">
      <input type="email" placeholder="Please enter your email" class="flex-grow mr-2" /><button type="submit" class="button">Join</button>
    </div>
  </div>
</div>
