<?php if ( get_theme_mod( 'show_logo_in_footer' ) ) : ?>
  <div class="border-b border-gray-500 p-6">
    <div class="m-auto w-48">
      <?php echo file_get_contents( get_stylesheet_directory_uri() . '/assets/footer_logo.svg' ); ?>
    </div>
  </div>
<?php endif; ?>

<div class="border-b border-gray-500 p-6">
  <nav class="text-sm">
    <?php
      // Styling is done on items in footer.css
      wp_nav_menu( array(
        'menu_class'      => 'footer-1-col-menu leading-loose text-center md:flex md:flex-wrap md:justify-center',
        'theme_location'  => 'footer-1'
      ) );
    ?>
  </nav>
</div>

<?php if ( get_theme_mod( 'show_social_in_footer' ) ) : ?>
  <div class="flex justify-center pl-6 pr-6 pt-6">
    <?php dgd_social_icons( false, false, 'text-gray-300 hover:text-white' ) ?>
  </div>
  <?php endif; ?>
  
<div id="credit" class="p-6 text-center text-xs">
  <div>&copy; 2020 Do Good Design Co. - All Rights Reserved | <?php dgd_credit(); ?></div>
</div>
<nav class="pb-6 pl-6 pr-6 text-xs">
  <?php
    wp_nav_menu( array(
      'menu_class'      => 'footer-1-col-menu leading-loose text-center md:flex md:flex-wrap md:justify-center',
      'theme_location' => 'sub-footer'
    ) );
  ?>
</nav>