<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package dgd-tailwind
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php dgd_tailwind_post_thumbnail(); ?>

	<?php dgd_page_header( get_the_title(), 1 ); ?>

	<div class="m-auto max-w-screen-lg">

		<div class="entry-content">
			<?php
			the_content();

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'dgd-tailwind' ),
				'after'  => '</div>',
			) );
			?>
		</div><!-- .entry-content -->

	</div>

</article><!-- #post-<?php the_ID(); ?> -->
