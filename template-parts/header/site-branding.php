<div class="site-branding flex-grow">
  <?php
    if ( is_front_page() && is_home() ) :
  ?>
    <h1 class="site-title mb-0"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
  <?php
    else :
  ?>
    <p class="site-title mb-0"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
  <?php
    endif;
    $dgd_tailwind_description = get_bloginfo( 'description', 'display' );
    if ( $dgd_tailwind_description || is_customize_preview() ) :
  ?>
    <p class="site-description hidden"><?php echo $dgd_tailwind_description; /* WPCS: xss ok. */ ?></p>
  <?php endif; ?>
</div><!-- .site-branding -->