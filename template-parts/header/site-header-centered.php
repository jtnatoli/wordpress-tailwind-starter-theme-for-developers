<header id="site-header" class="bg-blue-700 p-4 relative">
  <div class="xl:flex xl:justify-between xl:items-center">
    <?php get_template_part( 'template-parts/header/site-navigation' ); ?>
    <div class="flex">
      <?php get_template_part( 'template-parts/header/site-branding' ); ?>
      <?php get_template_part( 'template-parts/header/hamburger-button' ); ?>
    </div>
    <nav id="calls-to-action" class="mt-4 text-center lg:flex lg:justify-end lg:mt-0 lg:w-1/2">
      <?php
      wp_nav_menu( array(
        'menu_class'      => 'flex justify-center pr-2 lg:justify-end lg:p-0',
        'theme_location'  => 'calls-to-action',
      ) );
      ?>
    </nav>
  </div>
</header><!-- #masthead -->