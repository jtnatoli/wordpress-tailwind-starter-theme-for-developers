<nav id="site-navigation" class="lg:w-1/2">
  <div class="bg-blue-700 hidden menu-wrap pb-4 lg:block lg:pb-0">
    <?php
    wp_nav_menu( array(
      'theme_location' => 'primary',
    ) );
    ?>
  </div>
</nav><!-- #site-navigation -->