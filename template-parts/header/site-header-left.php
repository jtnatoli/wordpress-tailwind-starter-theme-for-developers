<header id="site-header" class="p-4">
  <div class="xl:flex xl:justify-between xl:items-center">
    <div class="flex">
      <?php get_template_part( 'template-parts/header/site-branding' ); ?>
      <?php get_template_part( 'template-parts/header/hamburger-button' ); ?>
    </div>
    <?php get_template_part( 'template-parts/header/site-navigation' ); ?>
  </div>
</header><!-- #masthead -->