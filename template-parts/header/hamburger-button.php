<div class="relative">
  <button id="site-navigation-toggle" class="border-none button-outline menu-toggle relative h-10 w-10 hover:bg-transparent" aria-controls="primary-menu" aria-expanded="false">
    <div class="bar-top"></div><div class="bar-mid"></div><div class="bar-bottom"></div>
    <span class="sr-only"><?php esc_html_e( 'Primary Menu', 'dgd-tailwind' ); ?></span>
  </button>
</div>