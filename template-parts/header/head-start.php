<?php
  if ( is_woocommerce_activated() ) {
    global $woocommerce;
    $cart_count = $woocommerce->cart->cart_contents_count;
  }
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class( 'font-body' ); ?>>
<div id="page" class="site">
	<a class="skip-link sr-only" href="#content"><?php esc_html_e( 'Skip to content', 'dgd-tailwind' ); ?></a>

	<?php if( get_theme_mod( 'top_bar_visibility' ) ) : ?>
		<div id="topbar" class="text-center px-2 py-2 text-xs">
			Complimentary Shipping and Free Samples with Every Order
		</div>
	<?php endif; ?>