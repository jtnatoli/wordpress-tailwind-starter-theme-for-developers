WordPress Tailwind Starter Theme for Developers
===

This is a work-in-progress that is added to as I complete projects using this as a starter. It was started with the Underscores theme.

This theme is intended to be a starting point for developers who want to use Tailwind in WordPress.

I've made an effort to use tailwind classes as much as possible and minimize the use off @apply in the css, however this isn't always possible. In the case of WooCommerce, I've favored using @apply as opposed to overwriting WooCommerce templates. 

I've made some additions to tailwind.config.js that I found to be useful. You may want to check this file out when getting started.

You should also look at css/custom_utilities.css.

Note: this is my first project using Webpack and nearly my first using Tailwind. I would love suggestions and hope this is useful.