<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package dgd-tailwind
 */
?>

<?php get_template_part( 'template-parts/header/head-start' ); ?>

<?php
	if ( get_theme_mod( 'header_style' ) === 'logo-left' ) {
		get_template_part( 'template-parts/header/site-header-left' );
	} else if ( get_theme_mod( 'header_style' ) === 'logo-centered' ) {
		get_template_part( 'template-parts/header/site-header-centered');
	}
?>

	<div id="content" class="site-content">
