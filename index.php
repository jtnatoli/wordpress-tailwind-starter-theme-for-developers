<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package dgd-tailwind
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php if ( have_posts() ) : ?>

			<?php dgd_page_header( get_the_archive_title(), 1, get_the_archive_description() ); ?>

			<div class="col-gap-8 grid grid-cols-3 m-auto max-w-screen-xl row-gap-8">

				<?php
				/* Start the Loop */
				while ( have_posts() ) :
					the_post();

					get_template_part( 'template-parts/blog/content', 'excerpt' );
				endwhile;
				?>

			</div>

			<?php 
			the_posts_navigation();

		else :

			get_template_part( 'template-parts/blog/content', 'none' );

		endif;
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
