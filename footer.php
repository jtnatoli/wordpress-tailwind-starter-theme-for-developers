<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package dgd-tailwind
 */

?>

	</div><!-- #content -->

	<footer id="site-footer" class="bg-gray-800 bg-dark text-gray-200">
		<?php
			$email_bar_style = get_theme_mod( 'email_bar_style' );
			if ( $email_bar_style === 'email_social' ) {
				get_template_part( 'template-parts/footer/email-bar-social');
			} else if ( $email_bar_style === 'email_only' ) {
				get_template_part( 'template-parts/footer/email-bar');
			}
		?>
		<?php 
			if ( get_theme_mod( 'footer_style' ) === '4-col' ) {
				get_template_part( 'template-parts/footer/footer-4-col' );
			} else if ( get_theme_mod( 'footer_style' ) === '1-col' ) {
				get_template_part( 'template-parts/footer/footer-1-col');
			}
		?>
	</footer><!-- #site-footer -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
