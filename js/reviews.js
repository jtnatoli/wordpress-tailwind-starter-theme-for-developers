jQuery( function( $ ) {

	// Check that this is the single product page
	if ( typeof wc_single_product_params === 'undefined' ) {
		return false;
	}

	// Click the first tab if the url contains a review hash, because
	// otherwise wc will activate the reviews tab that we removed
	// and all tabs will be hidden.
	$('.wc-tabs-wrapper, .woocommerce-tabs').ready( function(){
		var hash = window.location.hash.substring(1); //Puts hash in variable, and removes the # character
		var tabs = $( this ).find( '.wc-tabs, ul.tabs' ).first();
		if( hash.toLowerCase().indexOf( 'comment-' ) >=0 || hash == '#reviews' || hash == '#tab-reviews' ) {
			tabs.find( 'li:first a' ).click();
		}
	});

	// Rearrange the elements in the comment form for styling purposes.
	$('#commentform').ready( function(){
		$('#commentform').prepend('<h3>Please share your thoughts&hellip;</h3><div id="comment-form-formatter-inner" class="lg:flex"></div>');
		$wrapper = $('#comment-form-formatter-inner');
		$wrapper.append('<div class="left lg:mr-4 lg:w-3/5"></div><div class="right lg:ml-4 lg:w-2/5"></div>');
		$left = $('.left', $wrapper);
		$right = $('.right', $wrapper);
		$acfs = $('.acf-comment-fields');
		$form = $('#commentform');

		$('.comment-form-rating').appendTo($left);
		$('.comment-form-comment').appendTo($left);
		$('.comment-form-author').appendTo($right);
		$('.comment-form-email').appendTo($right);
		$acfs.appendTo($right);

		$('#commentform .form-submit').appendTo($form);

		$('.comment-notes').appendTo($form);

		$('#wp-comment-cookies-consent').click();
	});
});