import '../css/main.css';
import navigation from './navigation.js';
import skipLinkFocusFix from './skip-link-focus-fix.js';
import responsiveTabs from '../node_modules/responsive-tabs/js/jquery.responsiveTabs.min.js';
import reviews from './reviews.js';

jQuery( function($) {
	$('body').off( 'init', '.wc-tabs-wrapper, .woocommerce-tabs' );
	$('body').off( 'click', '.wc-tabs li a, ul.tabs li a' );

	$('.wc-tab').attr('style', null);

	$('.woocommerce-tabs').responsiveTabs({
	    startCollapsed: 'accordion'
	});
});