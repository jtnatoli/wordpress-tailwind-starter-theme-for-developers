<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package dgd-tailwind
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<?php if ( is_woocommerce_activated() ) : ?>
				<?php do_action( 'dgd_woocommerce_output_all_notices' ); ?>
				<?php
					if ( is_page(46) ) {
						echo '<div id="account_wrapper">';
						do_action( 'dgd_woocommerce_account_navigation' );
					}
				?>
			<?php endif; ?>
		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>
		<?php
			if ( is_page(46) && is_woocommerce_activated() ) {
				echo '</div>';
			}
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
