=== dgd-tailwind ===

Contributors: doogooddesignco
Tags: custom-menu, featured-images, threaded-comments, translation-ready

Requires at least: 4.5
Tested up to: 4.8
Stable tag: 1.0.0
License: GNU General Public License v2 or later
License URI: LICENSE

A starter theme called dgd-tailwind.

== Description ==

This is a work-in-progress that is added to as I complete projects using this as a starter. It was started with the Underscores theme.

This theme is intended to be a starting point for developers who want to use Tailwind in WordPress.

I've made an effort to use tailwind classes as much as possible and minimize the use off @apply in the css, however this isn't always possible. In the case of WooCommerce, I've favored using @apply as opposed to overwriting WooCommerce templates. 

I've made some additions to tailwind.config.js that I found to be useful. You may want to check this file out when getting started.

You should also look at css/custom_utilities.css.

Note: this is my first project using Webpack and nearly my first using Tailwind. I would love suggestions and hope this is useful.

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload Theme and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

== Changelog ==

= 1.0 - April 2 2020 =
* Initial release

== Credits ==

* Based on Underscores https://underscores.me/, (C) 2012-2017 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* normalize.css https://necolas.github.io/normalize.css/, (C) 2012-2016 Nicolas Gallagher and Jonathan Neal, [MIT](https://opensource.org/licenses/MIT)
* https://dogood.design
