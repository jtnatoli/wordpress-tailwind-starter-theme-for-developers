var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var BrowserSyncPlugin = require('browser-sync-webpack-plugin');

var extractPlugin = new ExtractTextPlugin({
	filename: 'style.css'
});

module.exports = {
	entry: './js/app.js',
	output: {
		path: path.resolve(__dirname, './'),
		filename: 'scripts.js',
	},
	module: {
		rules: [
			{
				test: /\.css$/,
				use: extractPlugin.extract({
					use: [
						{
							loader: 'css-loader',
						},
						{
							loader: 'postcss-loader',
							options: {
								ident: 'postcss',
								plugins: [
									require('postcss-import'),
									require('tailwindcss'),
									require('autoprefixer'),
								]
							}
						}
					]
				})
			},
			{
				test: /\.(svg|png|jpg|gif|woff)$/i,
				use: [
					{
						loader: 'file-loader',
						options: {
							outputPath: 'assets',
						}
					}
				]
			},
		]
	},
	plugins: [
		extractPlugin,
		new BrowserSyncPlugin({
      files: '**/*.php',
      proxy: 'http://opportunity-lab.local'
    })
	],
	watch: true,
};